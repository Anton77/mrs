main: function(slider,settings){
	this.root = slider;
	$.extend(this.data.settings,settings);
	var that = this, coords, arr = [];
		if(!this.data.settings.frozen){
			$(slider).find('.slider_line').on('mousedown',function(e){
				arr = [];
				coords = (e.offsetX===undefined)?e.pageX - $(e.target).offset().left:e.offsetX;
				// for(var i=0;i<that.data.list.length;i++){
				// 	arr.push(that.data.list[i].startX);
				// 	arr.push(that.data.list[i].endX);
				// }
				// console.log(Math.abs(that.closestTo(coords,arr,'closest')-coords));
				// if(Math.abs(that.closestTo(coords,arr,'closest')-coords)<13) return;
				that.create(coords);
			});
		}
		else {
			$(slider).find('.slider_line').css('cursor','default');
		}
		that.setWidth();
		$(window).on('resize',function(){
			that.setWidth.call(that);
		});
},