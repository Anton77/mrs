	var RangerX = function(){
			this.data = {
		settings: {
			step: 5,
			hours: 24,
			segInHour: 6,
			separator: 1,
			margin: 0,
			frozen: 0,
			showPrice: 1,
			nodeColor: '#0D6683',
			infoBgColor: '#0D6683',
			//infoTextColor: '#fff',
			//sideColor: '#fff',
			//sideWidth: '3px',
			callback: function(obj){return;}
		},
		tmp: {},
		nodes: 0,
		flags: [],
		list: []
	};
		this.main.apply(this,arguments);
	}

	RangerX.prototype = {
		main: function(slider,settings){
	this.root = slider;
	$.extend(this.data.settings,settings);
	var that = this, coords, arr = [];
		if(!this.data.settings.frozen){
			$(slider).find('.slider_line').on('mousedown',function(e){
				arr = [];
				coords = (e.offsetX===undefined)?e.pageX - $(e.target).offset().left:e.offsetX;
				// for(var i=0;i<that.data.list.length;i++){
				// 	arr.push(that.data.list[i].startX);
				// 	arr.push(that.data.list[i].endX);
				// }
				// console.log(Math.abs(that.closestTo(coords,arr,'closest')-coords));
				// if(Math.abs(that.closestTo(coords,arr,'closest')-coords)<13) return;
				that.create(coords);
			});
		}
		else {
			$(slider).find('.slider_line').css('cursor','default');
		}
		that.setWidth();
		$(window).on('resize',function(){
			that.setWidth.call(that);
		});
},
		switch: function(state){
	var that = this;
	if(state){
		if(this.data.list.length > 0){
			//запишем в базу
			localStorage.biranger = JSON.stringify(this.data.list);	
			
			//удалим все
			this.delete('all',function(){
				//создадим новый
				that.add([['0:00','24:00','0']],1);
			});
		}
		

	}
	else {
		if(localStorage.biranger){

			var arr = JSON.parse(localStorage.biranger);
			this.delete('all',function(){
				//пересоздадим ноды
				for(var i=0;i<arr.length;i++){
					//console.log('создадим #'+i+'___',arr[i].startX,arr[i].endX-arr[i].startX,arr[i].price);
					that.create(arr[i].startX,arr[i].endX-arr[i].startX,arr[i].price,2);					
				}
			});
		}
	}
},
		add: function(arr,state){
//exmple arr: .add([['10:10','15:30','1000']]);
	var sTime1 = null,
		sTime2 = null,
		eTime1 = null,
		eTime2 = null,
		price = null,
		sTimeArr = [],
		eTimeArr = [],
		id = [],
		startDot = null,
		size = null;


	for(var i=0;i<arr.length;i++){
		//подумать получше
		sTimeArr = arr[i][0].split(':');
		eTimeArr = arr[i][1].split(':');
		price = arr[i][2];

		sTime1 = sTimeArr[0];
		sTime2 = sTimeArr[1];

		eTime1 = eTimeArr[0];
		eTime2 = eTimeArr[1];

		//получим точку А
		startDot = (this.data.settings.step * this.data.settings.segInHour * (sTime1*1)) + ((sTime2*1)/10 * this.data.settings.step) + this.data.settings.margin;
		size = (sTime1 - eTime1) * this.data.settings.step * this.data.settings.segInHour + (((sTime2 - eTime2) / 10) * this.data.settings.step);
		id.push(this.create(startDot,Math.abs(size),price,(state)?state:2));
	}
		return id;
},
		create: function(where,size,val,state){
		//создадим загатовку
		var newNode = $('<div class="sl_line-box"> <div class="sl_line-section"> <div class="sl_line-section-left"></div><div class="sl_line-section-right"></div></div><div class="sl_line-tooltip"> <div class="tooltip_input"> <input class="tooltip_input-self" type="text"> <div class="tooltip_input-connector"></div></div><div class="tooltip_timeinfo"> <div class="tooltip_time-connector"></div><div class="tooltip_time-box"> <span> c <bdi>15:40</bdi> до <bdi>15:50</bdi> </span><i class="bui-cross tooltip_close"></i></div></div></div></div>'),
			//color = this.getColor(),
			nodeColor = this.data.settings.nodeColor,
			infoBgColor = this.data.settings.infoBgColor,
			that = this,
			line = newNode.find('.sl_line-section'),
			timeBox = newNode.find('.tooltip_time-box'),
			timeinfo = newNode.find('.tooltip_timeinfo'),
			input = newNode.find('.tooltip_input'),
			timeBoxW = null,
			inputVal = newNode.find('.tooltip_input-self');
			val = (val)?val:'0';

		//подключим базовую физику
		//this.physicInit(newNode);

		//присвоим id и атрибут для дальнейшего обращения к базе
		newNode.data().id = this.data.list.length;
		newNode.attr('id',this.data.list.length);

		//блокировка - заморозка + еще что-то
		if(!this.data.settings.frozen){
			line.on('mousedown',function(e){
				that.setPhys(this,e,'move');
			});
		}
		else {
			line.on('click',function(e){
				that.showTooltip($(this).parent());
			});
		}

		//блокировка - 
		if(state !== 1 && !this.data.settings.frozen){
			newNode.find('.sl_line-section-left').on('mousedown',function(e){
				that.setPhys(this,e,'left');
			});

			newNode.find('.sl_line-section-right').on('mousedown',function(e){
				that.setPhys(this,e,'right');
			});
		}

		//блокировка инпута и маркеров ресайза
		if(this.data.settings.frozen){
			inputVal.attr('disabled',true);
			newNode.find('.sl_line-section-left').css('cursor','default');
			newNode.find('.sl_line-section-right').css('cursor','default');
		}

		//ловушка для бабла
		newNode.on('mousedown',function(e){
			e.stopPropagation();
		});

		//расположим в месте клика:
		newNode.css('left',where+'px');

		//установим длину
		line.css('width',(size)?size+'px':this.data.settings.step*this.data.settings.segInHour+'px');

		//создадим новую запись в базе
		var listObj = {
			price: null,
			startX: null,
			endX: null
		};

		this.data.list.push(listObj);

		//calc start/end X
		this.updateCoords(newNode);

		//func >> база
		if(!that.data.list[newNode.data().id].price){
			that.data.list[newNode.data().id].price = val;
		}

		//func >> нода, разделим разряды
		inputVal.val(val);
		inputVal.val(inputVal.val().toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));

		//база UPD, разделим разряды
		inputVal.on('keyup',function(){
			that.data.list[newNode.data().id].price = this.value.replace(/[" "]/g, "");
            this.value = this.value.replace(/[" "]/g, "");
            this.value = this.value.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		});		

		//рандом цвет
		line.css('background',nodeColor);
		timeBox.css('background',infoBgColor);
		newNode.find('.tooltip_input-connector').css('background',infoBgColor);
		newNode.find('.tooltip_time-connector').css('background',infoBgColor);

		//возможность удаления
		if(state !== 1 && !this.data.settings.frozen){
			newNode.find('.tooltip_close').on('click',function(){
				that.delete($(this));
			});			
		}
		else {
			newNode.find('.tooltip_close').css('display','none');			
		}

		//режим без цифр
		if(!this.data.settings.showPrice){
			newNode.find('.tooltip_input').css('opacity','0');
			inputVal.attr('disabled',true);
		}

		//соединим в районе клика
		$(this.root).find('.slider_line').append(newNode); //!! плохой инвок

		//проверим центровку сепараторов
		timeBoxW = parseInt(timeBox.css('width'));
		if(parseInt(line.css('width'))<timeBoxW){
			timeinfo.css('left',parseInt(line.css('width'))/2-(timeBoxW/2)+'px');
			input.css('left',parseInt(line.css('width'))/2-(timeBoxW/2)+'px');
		}

		//установим время
		var width_m = parseInt(line.css('width')),
				dotA_m = parseInt(newNode.css('left')),
				dotB_m = dotA_m+width_m,
				time = this.getTime(dotA_m,dotB_m,parseInt($(this.root).find('.slider_line').css('width')));

		newNode.find('.tooltip_time-box bdi').eq(0).text(time[0][0]+':'+time[0][1]);
		newNode.find('.tooltip_time-box bdi').eq(1).text(time[1][0]+':'+time[1][1]);

		if(state !== 2){
			//фейдоут тултипов
			$('.slider_line').find('[active=true]').find('.tooltip_timeinfo').fadeOut('fast');
			$('.slider_line').find('[active=true]').find('.tooltip_input').fadeOut('fast');
			$('.slider_line').find('[active=true]').removeAttr('active');

			//attr active
			newNode.attr('active','true');

			//фейдин тултипов
			newNode.find('.tooltip_timeinfo').fadeIn('fast');
			newNode.find('.tooltip_input').fadeIn('fast');
		}

		this.data.settings.callback(newNode,'created');

		return this.data.nodes++;
},
		delete: function(el,callback){
	if(this.data.list.length === 0) {
		if(callback && cnt===length){
			callback.call(that);
		}
		return;
	}
	var box = (el==='all')?$('.sl_line-box'):el.parents('.sl_line-box'),
		id = (el==='all')?null:box.data().id,
		that = this,
		cnt = 0,
		length = box.length;
	box.fadeOut({
		duration: 'fast',
		complete:function(){
			cnt++;
			$(this).find('.sl_line-section-left').unbind('mousedown');
			$(this).find('.sl_line-section-right').unbind('mousedown');
			$(this).find('.sl_line-section').unbind('mousedown');
			(el==='all')?$(document).unbind('mouseup'):null;
			$(this).remove();
			(id===null)?that.data.list=[]:that.data.list[id] = 'deleted';
			if(callback && cnt===length){
				callback.call(that);
			}			
		}
	});
	this.data.settings.callback(box,(el==='all')?'deleted_all':'deleted');
},
		setPhys: function(node,e,type){
	e.stopPropagation();

	//vars
	var that = node,
		obj = this,
		flag = [],
		box = (type==='move')?$(node).parent():$(node).parent().parent(),
		line = (type==='move')?$(node):$(node).parent(),
		maxMove = parseInt($(node).parents('.slider_line').css('width'))-parseInt($(node).css('width')),
		maxSize = parseInt($(node).parents('.slider_line').css('width')) - parseInt(line.parents('.sl_line-box').css('left')),
		fullSize = parseInt(box.parent().css('width')),				
		//startposRoot = box.position().left,
		curMove = 0,
		curSize = 0,
		pos = 0,
		startWidth = parseInt(line.css('width')),
		id = box.data().id,
		width = parseInt(line.css('width')),
		dotA = parseInt(box.css('left')),
		dotB = dotA+width,
		time1 = box.find('.tooltip_time-box bdi').eq(0),
		time2 = box.find('.tooltip_time-box bdi').eq(1),
		timeBoxW = parseInt(line.next().find('.tooltip_time-box').css('width')),
		timeinfo = line.next().find('.tooltip_timeinfo'),
		input = line.next().find('.tooltip_input');

		if(type==='move'){

			//правая точка левого соседа
			var _N1 = obj.closestTo(dotA,1,id);
			_N1 = (_N1===undefined)?obj.data.settings.margin:_N1;
			var N1 = (dotA<_N1)?obj.data.settings.margin:_N1;
			N1 = (dotA<_N1)?obj.data.settings.margin:_N1;

			//левая точка правого соседа
			var _N2 = obj.closestTo(dotB,0,id);
			_N2 = (_N2===undefined)?obj.data.settings.margin:_N2;
			var N2 = (dotB>_N2)?maxMove+width-obj.data.settings.margin:_N2;

			//тултипы
			obj.showTooltip(box);

			$(document).unbind('mouseup');
			$(document).on('mouseup',function(){
				flag[0] = obj.stop('move',box,line,time1,time2,fullSize);
			});
			$(document).on('mousemove',function(e){
				e.stopPropagation();

				//установим время
				var width_m = parseInt($(that).css('width')),
				dotA_m = parseInt(box.css('left')),
				dotB_m = dotA_m+width_m;

				var arr = obj.getTime(dotA_m,dotB_m,fullSize);
				time1.text(arr[0][0]+':'+arr[0][1]);
				time2.text(arr[1][0]+':'+arr[1][1]);				

				if(!flag[0]){
					pos = e.clientX;
					flag[0]=1;
				}
				else {
					curMove = dotA+(e.clientX-pos);

						if(curMove<=N1){
							box.css({'left':N1+'px'});
							obj.updateCoords(box);
						}
						//ограничитель правого соседа/края
						else if((curMove+width)>=N2){
							box.css({'left':(N2-width)+'px'});
							obj.updateCoords(box);
						}
						//стандартный ход
						else {
							//console.log('case3');
							box.css({'left':curMove+'px'});
						}						
						//}


				}
				obj.data.settings.callback(box);
			});
		}

		else if(type==='left'){
			//правая точка левого соседа
			var _N1 = obj.closestTo(dotA,1,id);
			_N1 = (_N1===undefined)?obj.data.settings.margin:_N1;
			var N1 = (dotA<_N1)?obj.data.settings.margin:_N1;
			N1 = (dotA<_N1)?obj.data.settings.margin:_N1;

			//тултипы
			obj.showTooltip(box);			

			$(document).unbind('mouseup');
			$(document).on('mouseup',function(){
				flag[0] = obj.stop('left',box,line,time1,time2,fullSize);
			});

			$(document).on('mousemove',function(e){
				e.stopPropagation();
				
				curMove = dotA+(e.clientX-pos);
				curSize = startWidth-(e.clientX-pos);

				//установим время
				width_m = parseInt(line.css('width')),
				dotA_m = parseInt(box.css('left')),
				dotB_m = dotA_m+width_m;

				var arr = obj.getTime(dotA_m,dotB_m,fullSize);
				time1.text(arr[0][0]+':'+arr[0][1]);
				time2.text(arr[1][0]+':'+arr[1][1]);


				if(!flag[0]){
					pos = e.clientX;
					flag[0]=1;
				}
				else if(curSize <= obj.data.settings.step){
					line.css('width',obj.data.settings.step+'px');
					timeinfo.css('left',(Math.round(obj.data.settings.step/2))-(timeBoxW/2)+'px');
					input.css('left',(Math.round(obj.data.settings.step/2))-(timeBoxW/2)+'px');
				}
				else {
						//блок
						if(curMove<=N1){
							box.css({'left':N1+'px'});
							//obj.updateCoords(box);
						}

						//стандартный ход
						else {
							//console.log('case3');
							box.css({'left':curMove+'px'});
							line.css('width',(startWidth-(e.clientX-pos))+'px');
							obj.updateCoords(box);
							if(curSize < timeBoxW){
								timeinfo.css('left',(curSize/2)-(timeBoxW/2)+'px');
								input.css('left',(curSize/2)-(timeBoxW/2)+'px');
							}							
						}
				}

				obj.data.settings.callback(box);
				
				// if(!flag[0]){
				// 	pos = e.clientX;
				// 	flag[0]=1;
				// }
				// else if(curSize <= obj.data.settings.step){
				// 	line.css('width',obj.data.settings.step+'px');
				// 	timeinfo.css('left',(Math.round(obj.data.settings.step/2))-(timeBoxW/2)+'px');
				// 	input.css('left',(Math.round(obj.data.settings.step/2))-(timeBoxW/2)+'px');
				// }
				// else {
				// 	//if(curMove>=10){

				// 		// line.css('width',(startWidth-(e.clientX-pos))+'px');
				// 		// box.css({'left':curMove+'px'});
				// 		if(curMove>=N1){
				// 			box.css({'left':curMove+'px'});
				// 			line.css('width',(startWidth-(e.clientX-pos))+'px');
				// 			obj.updateCoords(box);
						

				// 		if(curSize < timeBoxW){
				// 			timeinfo.css('left',(curSize/2)-(timeBoxW/2)+'px');
				// 			input.css('left',(curSize/2)-(timeBoxW/2)+'px');
				// 		}
				// 	}
				// 	else {
				// 		box.css({'left':N1+'px'});
				// 		//box.css({'left':curMove+'px'});
				// 	}
				// }
				
			});			
		}

		else if(type==='right'){

			//левая точка правого соседа
			var _N2 = obj.closestTo(dotB,0,id);
			_N2 = (_N2===undefined)?obj.data.settings.margin:_N2;
			var N2 = (dotB>_N2)?fullSize-obj.data.settings.margin:_N2;
			//console.warn(N2);

			//тултипы
			obj.showTooltip(box);			

			$(document).unbind('mouseup');
			$(document).on('mouseup',function(){
				flag[0] = obj.stop('right',box,line,time1,time2,fullSize);
			});	

			$(document).on('mousemove',function(e){
				e.stopPropagation();
				curSize = startWidth+(e.clientX-pos);

				//установим время
				width_m = parseInt(line.css('width')),
				dotA_m = parseInt(box.css('left')),
				dotB_m = dotA_m+width_m;

				var arr = obj.getTime(dotA_m,dotB_m,fullSize);
				time1.text(arr[0][0]+':'+arr[0][1]);
				time2.text(arr[1][0]+':'+arr[1][1]);

				if(!flag[0]){
					pos = e.clientX;
					flag[0]=1;
				}
				else if(curSize <= obj.data.settings.step){
					line.css('width',obj.data.settings.step+'px');
					timeinfo.css('left',(Math.round(obj.data.settings.step/2))-(timeBoxW/2)+'px');
					input.css('left',(Math.round(obj.data.settings.step/2))-(timeBoxW/2)+'px');
				}				

				else {
					//curMove = dotA+(e.clientX-pos);

						//ограничитель правого соседа/края
						if(dotB_m>=N2){
							//box.css({'left':(N2-width)+'px'});
							line.css('width',N2-dotA_m+'px');
						}
						//стандартный ход
						else {
							//console.log('case3');
							//box.css({'left':curMove+'px'});
							line.css('width',curSize+'px');
							obj.updateCoords(box);
							if(curSize < timeBoxW){
								timeinfo.css('left',(curSize/2)-(timeBoxW/2)+'px');
								input.css('left',(curSize/2)-(timeBoxW/2)+'px');
							}
						}
						//}


				}	

				obj.data.settings.callback(box);

				// if(!flag[0]){
				// 	pos = e.clientX;
				// 	flag[0]=1;
				// }

				// /*блок налево*/
				// else if(curSize <= 5){
				// 	line.css('width','5px');
				// 	timeinfo.css('left',(2)-(timeBoxW/2)+'px');
				// 	input.css('left',(2)-(timeBoxW/2)+'px');
				// }
				// //else if(){/*блок направо*/}
				// //else{/*свободный ход*/}

				// //<<<<<<<<<<<<<<<<
				// else {
				// 	if(curSize <= maxSize - 10){
				// 		line.css('width',curSize+'px');
				// 		if(curSize < timeBoxW){
				// 			timeinfo.css('left',(curSize/2)-(timeBoxW/2)+'px');
				// 			input.css('left',(curSize/2)-(timeBoxW/2)+'px');
				// 		}
				// 	}
				// 	else {
				// 		line.css('width',(maxSize -10) + 'px');
				// 	}
				// }
				//>>>>>>>>>>>>>>>>>>



			});
		}
},
		updateCoords: function(item){
		//console.log('update');
		var id = item.data().id,
			left = parseInt(item.css('left')),
			//left = item.position().left,
			width = parseInt(item.find('.sl_line-section').css('width')),
			right = left + width;

		//предохранитель
		(this.data.list[id]==='deleted' || this.data.list[id]===undefined)?$(document).unbind('mouseup'):null;

		this.data.list[id].startX = left;
		this.data.list[id].endX = right;
},
		getPos: function(left,size){
		//console.log(Math.round(left/this.data.settings.step)*this.data.settings.step);
		return (size)?Math.round((left+size)/this.data.settings.step)*this.data.settings.step:Math.round(left/this.data.settings.step)*this.data.settings.step;
},
		getColor: function(){
		if(this.data.flags[0]){
			this.data.flags[0]=0;
			return 'rgb(0, 153, 204)';
		}
		else {
			this.data.flags[0]=1;
			return 'rgb(13, 102, 131)';
		}
},
		// getTime: function(left,right,fullSize){
// 		var hours = this.data.settings.hours,
// 			segments = this.data.settings.segInHour,
// 			total = hours * segments,
// 			step = this.data.settings.step,
// 			pos1 = this.getPos(left),
// 			pos2 = this.getPos(right),
// 			arr1 = ((pos1-10)/step/segments).toFixed(2).toString().split('.'),
// 			arr2 = ((pos2-10)/step/segments).toFixed(2).toString().split('.'),
// 			tArr = [],
// 			that = this;

			

// 			//console.log(((pos1-10)/step/segments).toFixed(2).toString().split('.'));
// 			if(arr1[0]==='0') arr1[0]='00';
// 			if(arr2[0]==='0') arr2[0]='00';
			
// 			if(arr1[1]){
// 				//console.log(arr1[1]*1);
// 				arr1[1] = this.timeSw(arr1[1]*1);
// 			}
// 			// else {
// 			// 	arr1[1] = '00';
// 			// }
// 			if(arr2[1]){
// 				arr2[1] = this.timeSw(arr2[1]*1);
// 			}
// 			// else {
// 			// 	arr2[1] = '00';
// 			// }
// 			tArr.push(arr1,arr2);
// 		return tArr; 
// },


getTime: function(left,right,fullSize){
	var hours = this.data.settings.hours,
		pxHour = (fullSize-this.data.settings.margin*2)/hours,

		pos1 = (left/pxHour).toFixed(2).toString().split('.'),
		pos2 = (right/pxHour).toFixed(2).toString().split('.'),
		all = [pos1,pos2];
		//console.log(pxStep);
		//console.log(((Math.round(all[0][1]/(100/this.data.settings.segInHour))-1)*10).toString());
		//console.log(all[1][1]);

		all[0][1] = ((Math.round(all[0][1]/(100/this.data.settings.segInHour)))*10).toString();
		all[1][1] = ((Math.round(all[1][1]/(100/this.data.settings.segInHour)))*10).toString();

		(all[0][1] === '60')?all[0][1] = '50':null;
		(all[0][1] === '0')?all[0][1] = '00':null;
		(all[1][1] === '60')?all[1][1] = '50':null;
		(all[1][1] === '0')?all[1][1] = '00':null;

		// all[0][1]=((Math.round((all[0][1]/1000*pxHour)-1))*10).toString();
		// all[1][1]=((Math.round((all[1][1]/1000*pxHour)-1))*10).toString();
		// (all[1][1]<0||all[1][1]==0)?all[1][1]='00':null;
		// (all[0][1]<0||all[0][1]==0)?all[0][1]='00':null;
		//all[0][1] = this.timeSw(all[0][1]);
		//all[1][1] = this.timeSw(all[1][1]);

		//console.log(left/pxHour);
		//console.log('second '+all[1][1]);

		return all;
},
		getData: function(){
	var arr = [],
		tArr = [];
	for(var i=0;i<this.data.list.length;i++){
		tArr = this.getTime(this.data.list[i].startX,this.data.list[i].endX,parseInt($(this.root).find('.slider_line').css('width')));
		tArr.push(this.data.list[i].price);
		arr.push(tArr);

	}

	for(var i=0;i<arr.length;i++){
		for(var j=0;j<arr[i].length;j++){
			if(typeof arr[i][j] === 'object'){
				arr[i][j] = arr[i][j].join(':');
			}
		}
	}
	return arr;
},
		closestTo: function(number,type,id) {
		var set = [];
		if(id==='closest'){
			set = type;
		}
		else {
			if(type){
				//массив B(1)
				for(var i=0;i<this.data.list.length;i++){
					if(id===i || this.data.list[i]==='deleted' || this.data.list[i].endX>number) continue;
					set.push(this.data.list[i].endX);
				}
			}
			else {
				//массив A (0)
				for(var i=0;i<this.data.list.length;i++){
					if(id===i || this.data.list[i]==='deleted' || this.data.list[i].startX<number) continue;
					set.push(this.data.list[i].startX);
				}			
			}			
		}

    	var closest = set[0],
    	prev = Math.abs(set[0] - number);
	    for (var i = 1; i < set.length; i++) {
	        var diff = Math.abs(set[i] - number);

	        if (diff < prev) {
	            prev = diff;
	            closest = set[i];
	        }
	    }
	    //(!type)?console.log('right: number/set/founded',number,set,closest):null;	    
	    return closest;
},
		//отключим move,обновим координаты,сделаем автодоводку

stop: function(type,box,line,time1,time2,fullSize){
	$(document).unbind('mousemove');
	if(type==='left'){
		line.css('width',this.getPos(parseInt(line.css('width')))+'px');
	}
	if(type==='right'){
		line.css('width',this.getPos(parseInt(line.css('width')))+'px');	
	}
	
	box.css({'left':this.getPos(parseInt(box.css('left')))+'px'});

	this.updateCoords(box);

	//установим время
	width_m = parseInt(line.css('width')),
	dotA_m = parseInt(box.css('left')),
	dotB_m = dotA_m+width_m;

	var arr = this.getTime(dotA_m,dotB_m,fullSize);
	time1.text(arr[0][0]+':'+arr[0][1]);
	time2.text(arr[1][0]+':'+arr[1][1]);

	//callback sync
	this.data.settings.callback(box);

	return 0;
},
		showTooltip: function(box){
	if(!box.attr('active')){
		//зачистим тултипы
		box.parent().find('[active=true]').find('.tooltip_timeinfo').fadeOut('fast');
		box.parent().find('[active=true]').find('.tooltip_input').fadeOut('fast');
		box.parent().find('[active=true]').removeAttr('active');

		box.attr('active',true);

		//покажем выбранные
		box.find('.tooltip_timeinfo').fadeIn('fast');
		box.find('.tooltip_input').fadeIn('fast');
	}	
},
		getWidth: function(){
	var width = parseInt($(this.root).find('.slider_line').parent().css('width')),
		tWidth = 0,
		i = 1;
	for(var i=1,tWidth=0;width>tWidth;i++){
		tWidth = (i*6*24);
	}
	return ((i-3)<4)?4:i-3;
},
		setWidth: function(){
			var width_i = this.getWidth();
			this.data.settings.step = width_i;
			$(this.root).find('.sl_grid-normal').css('margin-left',width_i-1+'px');
			$(this.root).find('.sl_grid-big').css('margin-left',width_i-1+'px');
			$(this.root).find('.slider_line').css('width',(width_i*6*24+this.data.settings.margin*2)+'px');
			$(this.root).find('.slider_numbers').find('span').not(":first").css('margin-left',(((width_i*6*24)/24)-20)+'px');
			$(this.root).find('.slider_numbers').find('span').first().css('margin-left','0px');
			$(this.root).find('.sl_grid-big').first().css('margin-left','0px');
}
	}
