setPhys: function(node,e,type){
	e.stopPropagation();

	//vars
	var that = node,
		obj = this,
		flag = [],
		box = (type==='move')?$(node).parent():$(node).parent().parent(),
		line = (type==='move')?$(node):$(node).parent(),
		maxMove = parseInt($(node).parents('.slider_line').css('width'))-parseInt($(node).css('width')),
		maxSize = parseInt($(node).parents('.slider_line').css('width')) - parseInt(line.parents('.sl_line-box').css('left')),
		fullSize = parseInt(box.parent().css('width')),				
		//startposRoot = box.position().left,
		curMove = 0,
		curSize = 0,
		pos = 0,
		startWidth = parseInt(line.css('width')),
		id = box.data().id,
		width = parseInt(line.css('width')),
		dotA = parseInt(box.css('left')),
		dotB = dotA+width,
		time1 = box.find('.tooltip_time-box bdi').eq(0),
		time2 = box.find('.tooltip_time-box bdi').eq(1),
		timeBoxW = parseInt(line.next().find('.tooltip_time-box').css('width')),
		timeinfo = line.next().find('.tooltip_timeinfo'),
		input = line.next().find('.tooltip_input');

		if(type==='move'){

			//правая точка левого соседа
			var _N1 = obj.closestTo(dotA,1,id);
			_N1 = (_N1===undefined)?obj.data.settings.margin:_N1;
			var N1 = (dotA<_N1)?obj.data.settings.margin:_N1;
			N1 = (dotA<_N1)?obj.data.settings.margin:_N1;

			//левая точка правого соседа
			var _N2 = obj.closestTo(dotB,0,id);
			_N2 = (_N2===undefined)?obj.data.settings.margin:_N2;
			var N2 = (dotB>_N2)?maxMove+width-obj.data.settings.margin:_N2;

			//тултипы
			obj.showTooltip(box);

			$(document).unbind('mouseup');
			$(document).on('mouseup',function(){
				flag[0] = obj.stop('move',box,line,time1,time2,fullSize);
			});
			$(document).on('mousemove',function(e){
				e.stopPropagation();

				//установим время
				var width_m = parseInt($(that).css('width')),
				dotA_m = parseInt(box.css('left')),
				dotB_m = dotA_m+width_m;

				var arr = obj.getTime(dotA_m,dotB_m,fullSize);
				time1.text(arr[0][0]+':'+arr[0][1]);
				time2.text(arr[1][0]+':'+arr[1][1]);				

				if(!flag[0]){
					pos = e.clientX;
					flag[0]=1;
				}
				else {
					curMove = dotA+(e.clientX-pos);

						if(curMove<=N1){
							box.css({'left':N1+'px'});
							obj.updateCoords(box);
						}
						//ограничитель правого соседа/края
						else if((curMove+width)>=N2){
							box.css({'left':(N2-width)+'px'});
							obj.updateCoords(box);
						}
						//стандартный ход
						else {
							//console.log('case3');
							box.css({'left':curMove+'px'});
						}						
						//}


				}
				obj.data.settings.callback(box);
			});
		}

		else if(type==='left'){
			//правая точка левого соседа
			var _N1 = obj.closestTo(dotA,1,id);
			_N1 = (_N1===undefined)?obj.data.settings.margin:_N1;
			var N1 = (dotA<_N1)?obj.data.settings.margin:_N1;
			N1 = (dotA<_N1)?obj.data.settings.margin:_N1;

			//тултипы
			obj.showTooltip(box);			

			$(document).unbind('mouseup');
			$(document).on('mouseup',function(){
				flag[0] = obj.stop('left',box,line,time1,time2,fullSize);
			});

			$(document).on('mousemove',function(e){
				e.stopPropagation();
				
				curMove = dotA+(e.clientX-pos);
				curSize = startWidth-(e.clientX-pos);

				//установим время
				width_m = parseInt(line.css('width')),
				dotA_m = parseInt(box.css('left')),
				dotB_m = dotA_m+width_m;

				var arr = obj.getTime(dotA_m,dotB_m,fullSize);
				time1.text(arr[0][0]+':'+arr[0][1]);
				time2.text(arr[1][0]+':'+arr[1][1]);


				if(!flag[0]){
					pos = e.clientX;
					flag[0]=1;
				}
				else if(curSize <= obj.data.settings.step){
					line.css('width',obj.data.settings.step+'px');
					timeinfo.css('left',(Math.round(obj.data.settings.step/2))-(timeBoxW/2)+'px');
					input.css('left',(Math.round(obj.data.settings.step/2))-(timeBoxW/2)+'px');
				}
				else {
						//блок
						if(curMove<=N1){
							box.css({'left':N1+'px'});
							//obj.updateCoords(box);
						}

						//стандартный ход
						else {
							//console.log('case3');
							box.css({'left':curMove+'px'});
							line.css('width',(startWidth-(e.clientX-pos))+'px');
							obj.updateCoords(box);
							if(curSize < timeBoxW){
								timeinfo.css('left',(curSize/2)-(timeBoxW/2)+'px');
								input.css('left',(curSize/2)-(timeBoxW/2)+'px');
							}							
						}
				}

				obj.data.settings.callback(box);
				
				// if(!flag[0]){
				// 	pos = e.clientX;
				// 	flag[0]=1;
				// }
				// else if(curSize <= obj.data.settings.step){
				// 	line.css('width',obj.data.settings.step+'px');
				// 	timeinfo.css('left',(Math.round(obj.data.settings.step/2))-(timeBoxW/2)+'px');
				// 	input.css('left',(Math.round(obj.data.settings.step/2))-(timeBoxW/2)+'px');
				// }
				// else {
				// 	//if(curMove>=10){

				// 		// line.css('width',(startWidth-(e.clientX-pos))+'px');
				// 		// box.css({'left':curMove+'px'});
				// 		if(curMove>=N1){
				// 			box.css({'left':curMove+'px'});
				// 			line.css('width',(startWidth-(e.clientX-pos))+'px');
				// 			obj.updateCoords(box);
						

				// 		if(curSize < timeBoxW){
				// 			timeinfo.css('left',(curSize/2)-(timeBoxW/2)+'px');
				// 			input.css('left',(curSize/2)-(timeBoxW/2)+'px');
				// 		}
				// 	}
				// 	else {
				// 		box.css({'left':N1+'px'});
				// 		//box.css({'left':curMove+'px'});
				// 	}
				// }
				
			});			
		}

		else if(type==='right'){

			//левая точка правого соседа
			var _N2 = obj.closestTo(dotB,0,id);
			_N2 = (_N2===undefined)?obj.data.settings.margin:_N2;
			var N2 = (dotB>_N2)?fullSize-obj.data.settings.margin:_N2;
			//console.warn(N2);

			//тултипы
			obj.showTooltip(box);			

			$(document).unbind('mouseup');
			$(document).on('mouseup',function(){
				flag[0] = obj.stop('right',box,line,time1,time2,fullSize);
			});	

			$(document).on('mousemove',function(e){
				e.stopPropagation();
				curSize = startWidth+(e.clientX-pos);

				//установим время
				width_m = parseInt(line.css('width')),
				dotA_m = parseInt(box.css('left')),
				dotB_m = dotA_m+width_m;

				var arr = obj.getTime(dotA_m,dotB_m,fullSize);
				time1.text(arr[0][0]+':'+arr[0][1]);
				time2.text(arr[1][0]+':'+arr[1][1]);

				if(!flag[0]){
					pos = e.clientX;
					flag[0]=1;
				}
				else if(curSize <= obj.data.settings.step){
					line.css('width',obj.data.settings.step+'px');
					timeinfo.css('left',(Math.round(obj.data.settings.step/2))-(timeBoxW/2)+'px');
					input.css('left',(Math.round(obj.data.settings.step/2))-(timeBoxW/2)+'px');
				}				

				else {
					//curMove = dotA+(e.clientX-pos);

						//ограничитель правого соседа/края
						if(dotB_m>=N2){
							//box.css({'left':(N2-width)+'px'});
							line.css('width',N2-dotA_m+'px');
						}
						//стандартный ход
						else {
							//console.log('case3');
							//box.css({'left':curMove+'px'});
							line.css('width',curSize+'px');
							obj.updateCoords(box);
							if(curSize < timeBoxW){
								timeinfo.css('left',(curSize/2)-(timeBoxW/2)+'px');
								input.css('left',(curSize/2)-(timeBoxW/2)+'px');
							}
						}
						//}


				}	

				obj.data.settings.callback(box);

				// if(!flag[0]){
				// 	pos = e.clientX;
				// 	flag[0]=1;
				// }

				// /*блок налево*/
				// else if(curSize <= 5){
				// 	line.css('width','5px');
				// 	timeinfo.css('left',(2)-(timeBoxW/2)+'px');
				// 	input.css('left',(2)-(timeBoxW/2)+'px');
				// }
				// //else if(){/*блок направо*/}
				// //else{/*свободный ход*/}

				// //<<<<<<<<<<<<<<<<
				// else {
				// 	if(curSize <= maxSize - 10){
				// 		line.css('width',curSize+'px');
				// 		if(curSize < timeBoxW){
				// 			timeinfo.css('left',(curSize/2)-(timeBoxW/2)+'px');
				// 			input.css('left',(curSize/2)-(timeBoxW/2)+'px');
				// 		}
				// 	}
				// 	else {
				// 		line.css('width',(maxSize -10) + 'px');
				// 	}
				// }
				//>>>>>>>>>>>>>>>>>>



			});
		}
},