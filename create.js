create: function(where,size,val,state){
		//создадим загатовку
		var newNode = $('<div class="sl_line-box"> <div class="sl_line-section"> <div class="sl_line-section-left"></div><div class="sl_line-section-right"></div></div><div class="sl_line-tooltip"> <div class="tooltip_input"> <input class="tooltip_input-self" type="text"> <div class="tooltip_input-connector"></div></div><div class="tooltip_timeinfo"> <div class="tooltip_time-connector"></div><div class="tooltip_time-box"> <span> c <bdi>15:40</bdi> до <bdi>15:50</bdi> </span><i class="bui-cross tooltip_close"></i></div></div></div></div>'),
			//color = this.getColor(),
			nodeColor = this.data.settings.nodeColor,
			infoBgColor = this.data.settings.infoBgColor,
			that = this,
			line = newNode.find('.sl_line-section'),
			timeBox = newNode.find('.tooltip_time-box'),
			timeinfo = newNode.find('.tooltip_timeinfo'),
			input = newNode.find('.tooltip_input'),
			timeBoxW = null,
			inputVal = newNode.find('.tooltip_input-self');
			val = (val)?val:'0';

		//подключим базовую физику
		//this.physicInit(newNode);

		//присвоим id и атрибут для дальнейшего обращения к базе
		newNode.data().id = this.data.list.length;
		newNode.attr('id',this.data.list.length);

		//блокировка - заморозка + еще что-то
		if(!this.data.settings.frozen){
			line.on('mousedown',function(e){
				that.setPhys(this,e,'move');
			});
		}
		else {
			line.on('click',function(e){
				that.showTooltip($(this).parent());
			});
		}

		//блокировка - 
		if(state !== 1 && !this.data.settings.frozen){
			newNode.find('.sl_line-section-left').on('mousedown',function(e){
				that.setPhys(this,e,'left');
			});

			newNode.find('.sl_line-section-right').on('mousedown',function(e){
				that.setPhys(this,e,'right');
			});
		}

		//блокировка инпута и маркеров ресайза
		if(this.data.settings.frozen){
			inputVal.attr('disabled',true);
			newNode.find('.sl_line-section-left').css('cursor','default');
			newNode.find('.sl_line-section-right').css('cursor','default');
		}

		//ловушка для бабла
		newNode.on('mousedown',function(e){
			e.stopPropagation();
		});

		//расположим в месте клика:
		newNode.css('left',where+'px');

		//установим длину
		line.css('width',(size)?size+'px':this.data.settings.step*this.data.settings.segInHour+'px');

		//создадим новую запись в базе
		var listObj = {
			price: null,
			startX: null,
			endX: null
		};

		this.data.list.push(listObj);

		//calc start/end X
		this.updateCoords(newNode);

		//func >> база
		if(!that.data.list[newNode.data().id].price){
			that.data.list[newNode.data().id].price = val;
		}

		//func >> нода, разделим разряды
		inputVal.val(val);
		inputVal.val(inputVal.val().toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));

		//база UPD, разделим разряды
		inputVal.on('keyup',function(){
			that.data.list[newNode.data().id].price = this.value.replace(/[" "]/g, "");
            this.value = this.value.replace(/[" "]/g, "");
            this.value = this.value.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		});		

		//рандом цвет
		line.css('background',nodeColor);
		timeBox.css('background',infoBgColor);
		newNode.find('.tooltip_input-connector').css('background',infoBgColor);
		newNode.find('.tooltip_time-connector').css('background',infoBgColor);

		//возможность удаления
		if(state !== 1 && !this.data.settings.frozen){
			newNode.find('.tooltip_close').on('click',function(){
				that.delete($(this));
			});			
		}
		else {
			newNode.find('.tooltip_close').css('display','none');			
		}

		//режим без цифр
		if(!this.data.settings.showPrice){
			newNode.find('.tooltip_input').css('opacity','0');
			inputVal.attr('disabled',true);
		}

		//соединим в районе клика
		$(this.root).find('.slider_line').append(newNode); //!! плохой инвок

		//проверим центровку сепараторов
		timeBoxW = parseInt(timeBox.css('width'));
		if(parseInt(line.css('width'))<timeBoxW){
			timeinfo.css('left',parseInt(line.css('width'))/2-(timeBoxW/2)+'px');
			input.css('left',parseInt(line.css('width'))/2-(timeBoxW/2)+'px');
		}

		//установим время
		var width_m = parseInt(line.css('width')),
				dotA_m = parseInt(newNode.css('left')),
				dotB_m = dotA_m+width_m,
				time = this.getTime(dotA_m,dotB_m,parseInt($(this.root).find('.slider_line').css('width')));

		newNode.find('.tooltip_time-box bdi').eq(0).text(time[0][0]+':'+time[0][1]);
		newNode.find('.tooltip_time-box bdi').eq(1).text(time[1][0]+':'+time[1][1]);

		if(state !== 2){
			//фейдоут тултипов
			$('.slider_line').find('[active=true]').find('.tooltip_timeinfo').fadeOut('fast');
			$('.slider_line').find('[active=true]').find('.tooltip_input').fadeOut('fast');
			$('.slider_line').find('[active=true]').removeAttr('active');

			//attr active
			newNode.attr('active','true');

			//фейдин тултипов
			newNode.find('.tooltip_timeinfo').fadeIn('fast');
			newNode.find('.tooltip_input').fadeIn('fast');
		}

		this.data.settings.callback(newNode,'created');

		return this.data.nodes++;
},