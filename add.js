add: function(arr,state){
//exmple arr: .add([['10:10','15:30','1000']]);
	var sTime1 = null,
		sTime2 = null,
		eTime1 = null,
		eTime2 = null,
		price = null,
		sTimeArr = [],
		eTimeArr = [],
		id = [],
		startDot = null,
		size = null;


	for(var i=0;i<arr.length;i++){
		//подумать получше
		sTimeArr = arr[i][0].split(':');
		eTimeArr = arr[i][1].split(':');
		price = arr[i][2];

		sTime1 = sTimeArr[0];
		sTime2 = sTimeArr[1];

		eTime1 = eTimeArr[0];
		eTime2 = eTimeArr[1];

		//получим точку А
		startDot = (this.data.settings.step * this.data.settings.segInHour * (sTime1*1)) + ((sTime2*1)/10 * this.data.settings.step) + this.data.settings.margin;
		size = (sTime1 - eTime1) * this.data.settings.step * this.data.settings.segInHour + (((sTime2 - eTime2) / 10) * this.data.settings.step);
		id.push(this.create(startDot,Math.abs(size),price,(state)?state:2));
	}
		return id;
},