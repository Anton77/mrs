showTooltip: function(box){
	if(!box.attr('active')){
		//зачистим тултипы
		box.parent().find('[active=true]').find('.tooltip_timeinfo').fadeOut('fast');
		box.parent().find('[active=true]').find('.tooltip_input').fadeOut('fast');
		box.parent().find('[active=true]').removeAttr('active');

		box.attr('active',true);

		//покажем выбранные
		box.find('.tooltip_timeinfo').fadeIn('fast');
		box.find('.tooltip_input').fadeIn('fast');
	}	
},