setWidth: function(){
			var width_i = this.getWidth();
			this.data.settings.step = width_i;
			$(this.root).find('.sl_grid-normal').css('margin-left',width_i-1+'px');
			$(this.root).find('.sl_grid-big').css('margin-left',width_i-1+'px');
			$(this.root).find('.slider_line').css('width',(width_i*6*24+this.data.settings.margin*2)+'px');
			$(this.root).find('.slider_numbers').find('span').not(":first").css('margin-left',(((width_i*6*24)/24)-20)+'px');
			$(this.root).find('.slider_numbers').find('span').first().css('margin-left','0px');
			$(this.root).find('.sl_grid-big').first().css('margin-left','0px');
}