closestTo: function(number,type,id) {
		var set = [];
		if(id==='closest'){
			set = type;
		}
		else {
			if(type){
				//массив B(1)
				for(var i=0;i<this.data.list.length;i++){
					if(id===i || this.data.list[i]==='deleted' || this.data.list[i].endX>number) continue;
					set.push(this.data.list[i].endX);
				}
			}
			else {
				//массив A (0)
				for(var i=0;i<this.data.list.length;i++){
					if(id===i || this.data.list[i]==='deleted' || this.data.list[i].startX<number) continue;
					set.push(this.data.list[i].startX);
				}			
			}			
		}

    	var closest = set[0],
    	prev = Math.abs(set[0] - number);
	    for (var i = 1; i < set.length; i++) {
	        var diff = Math.abs(set[i] - number);

	        if (diff < prev) {
	            prev = diff;
	            closest = set[i];
	        }
	    }
	    //(!type)?console.log('right: number/set/founded',number,set,closest):null;	    
	    return closest;
},