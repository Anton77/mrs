	var RangerX = function(){
		@import 'database.js';
		this.main.apply(this,arguments);
	}

	RangerX.prototype = {
		@import 'main.js';
		@import 'switch.js';
		@import 'add.js';
		@import 'create.js';
		@import 'delete.js';
		@import 'setPhys.js';
		@import 'updateCoords.js';
		@import 'getPos.js';
		@import 'getColor.js';
		@import 'getTime.js';
		@import 'getData.js';
		@import 'closestTo.js';
		@import 'stop.js';
		@import 'showTooltip.js';
		@import 'getWidth.js';
		@import 'setWidth.js';
	}
