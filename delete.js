delete: function(el,callback){
	if(this.data.list.length === 0) {
		if(callback && cnt===length){
			callback.call(that);
		}
		return;
	}
	var box = (el==='all')?$('.sl_line-box'):el.parents('.sl_line-box'),
		id = (el==='all')?null:box.data().id,
		that = this,
		cnt = 0,
		length = box.length;
	box.fadeOut({
		duration: 'fast',
		complete:function(){
			cnt++;
			$(this).find('.sl_line-section-left').unbind('mousedown');
			$(this).find('.sl_line-section-right').unbind('mousedown');
			$(this).find('.sl_line-section').unbind('mousedown');
			(el==='all')?$(document).unbind('mouseup'):null;
			$(this).remove();
			(id===null)?that.data.list=[]:that.data.list[id] = 'deleted';
			if(callback && cnt===length){
				callback.call(that);
			}			
		}
	});
	this.data.settings.callback(box,(el==='all')?'deleted_all':'deleted');
},