//отключим move,обновим координаты,сделаем автодоводку

stop: function(type,box,line,time1,time2,fullSize){
	$(document).unbind('mousemove');
	if(type==='left'){
		line.css('width',this.getPos(parseInt(line.css('width')))+'px');
	}
	if(type==='right'){
		line.css('width',this.getPos(parseInt(line.css('width')))+'px');	
	}
	
	box.css({'left':this.getPos(parseInt(box.css('left')))+'px'});

	this.updateCoords(box);

	//установим время
	width_m = parseInt(line.css('width')),
	dotA_m = parseInt(box.css('left')),
	dotB_m = dotA_m+width_m;

	var arr = this.getTime(dotA_m,dotB_m,fullSize);
	time1.text(arr[0][0]+':'+arr[0][1]);
	time2.text(arr[1][0]+':'+arr[1][1]);

	//callback sync
	this.data.settings.callback(box);

	return 0;
},