updateCoords: function(item){
		//console.log('update');
		var id = item.data().id,
			left = parseInt(item.css('left')),
			//left = item.position().left,
			width = parseInt(item.find('.sl_line-section').css('width')),
			right = left + width;

		//предохранитель
		(this.data.list[id]==='deleted' || this.data.list[id]===undefined)?$(document).unbind('mouseup'):null;

		this.data.list[id].startX = left;
		this.data.list[id].endX = right;
},